<?php

include_once("classes/Database.php");

// Récupérer les données du formulaire ($_POST)
$email = $_POST["email"];
$name = $_POST["name"];
$password = $_POST["password"];
$birthdate = $_POST["birthdate"];

// Instancier un objet Database
$database = new Database();

// Vérifier que l'email d'inscription est unique
$nbEmail = $database->getEmailOccurences($email);
if($nbEmail > 0){
    $error = "Cet email est déjà utilisé";
    header("location:inscription.php?error=$error");
    exit;
}

// Hasher le mot de passe et le stocker dans une nouvelle variable (fonction password_hash).
$passwordHash = password_hash($password, PASSWORD_DEFAULT);


// Appeler la fonction inscription en lui passant les données du formulaire d’incription ainsi que le mot de passe hashé au lieu du mot de passe en clair.
$id = $database->inscription($email, $name, $passwordHash, $birthdate);

//var_dump($id);

// Rediriger vers la page de login.
header("location:login.php");


?>