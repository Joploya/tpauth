<?php

require_once("classes/Database.php");

// Récupérer l’email et le mot de passe du formulaire ($_POST)
$email = $_POST["email"];
$password = $_POST["password"];


// Instancier une nouvelle Database et appeler la fonction getUserByEmail en lui passant l’email précédemment récupéré. 
// Cette fonction nous renvoie toutes les informations dans un objet User qu’il faut stocker dans une nouvelle variable.
$database = new Database();
$user = $database->getUserByEmail($email);

// Vérifiez que le mot de passe (user->getPassword()) du user n’est pas null. 
// Si c’est le cas, renvoyer (header…) sur la page login.
if($user == null){
    $error = "L'email utilisé n'a pas été trouvé.";
    header("location:login.php?error=$error");
    exit;
}
$passwordHash = $user->getPassword();
if($passwordHash == null){
    $error = "Problème recontré lors de la recherche du mot de passe.";
    header("location:login.php?error=$error");
    exit;
}


// Authentifiez l’utilisateur grâce au mot de passe entré dans le formulaire, 
// en le comparant au mot de passe de l’objet User, à l’aide de la fonction password_verify.
$authenticate = password_verify($password, $passwordHash);

//var_dump($authenticate);


// Si la fonction password_verify retourne true, stockez l’id du user dans la session ($_SESSION) 
// et redirigez vers la page perso.php sinon renvoyez si la page login.php
if($authenticate == true){
    session_start();
    $_SESSION["id"] = $user->getId();
    header("location:perso.php");
    exit;
}

$error = "Le mot de passe n'est pas valide";
header("location:login.php?error=$error");



?>