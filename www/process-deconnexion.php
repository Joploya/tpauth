<?php
// démarrer automatiquement la session au début de la page
session_start();

// appelez simplement la fonction session_destroy 
session_destroy();

// puis redirigez vers la page login.
header("location:login.php");

?>