<?php
require_once("classes/Database.php");

// Démarrer la session (session_start)
session_start();

// Récupérer l’id stocké dans la session ($_SESSION[‘’id’’])
$id = $_SESSION["id"];

// Vérifier que l’id n’est pas null ou vide. Si c’est le cas il faut renvoyer vers la page de login.
if(!isset($id)){
    header("location:login.php");
}


// Instancier une nouvelle Database et récupérer l’utilisateur en fonction de l’id de la session (getUserById). 
// Stocker l’utiisateur dans une nouvelle variable.
$database = new Database();
$user = $database->getUserById($id);


// Il faut également stocker dans une variable le nombre de jour depuis la naissance de l’utilisateur. Pour cela :
// // Récupérer la date d’aujourd’hui
// // Trouver une fonction ou en écrire une qui donne le nombre de jour entre 2 dates
// // Stocker le résultat dans une nouvelle variable
$today = new DateTime();
$birthdate = new DateTime($user->getBirthdate());

$difference = $today->diff($birthdate);

$diffInDays = $difference->format("%a");

?>
<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Main CSS -->
    <link rel="stylesheet" href="style.css">
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

    <title>TP AUTH</title>
  </head>
  <body>
    <h1 class="titre">Page personnelle</h1>

    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <p>Bienvenue <?php echo $user->getName(); ?>, vous êtes sur terre depuis <?php echo $diffInDays ?> jours.</p>
            </div>
            <div class="col-sm-12">
                <form action="process-deconnexion.php" method="POST">
                    <button type="submit" class="btn btn-outline-danger">Se déconnecter</button>
                </form>
            </div>
        </div>
    </div>

    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
  </body>
</html>