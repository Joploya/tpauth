<?php

require_once("User.php");

class Database{
    // Attributs
    private $connexion;

    // Le constructeur
    public function __construct(){
        $PARAM_hote = "mariadb";
        $PARAM_port = "3306";
        $PARAM_nom_bd = "tpAuth";
        $PARAM_utilisateur = "adminAuth";
        $PARAM_mot_passe = "adm1n@th";

        try{
            // Le code qu'on essaye de faire
            $this->connexion = new PDO('mysql:dbname='.$PARAM_nom_bd.';host='.$PARAM_hote,
                                $PARAM_utilisateur,
                                $PARAM_mot_passe);
        }catch(Exception $monException){
            echo "Erreur : ".$monException->getMessage()."<br/>";
            echo "Code : ".$monException->getCode();
        }

    }

    // Affiche le nom de la base de données utilisée
    public function getDBName(){
        return $this->connexion->query('select database()')->fetchColumn();
    }

    // Les fonctions 
    // Permet de voir si l'attribut connexion est bien initialisé
    public function getConnexion(){
        return $this->connexion;
    }

    // Cette fonction permet d’insérer un nouvel utilisateur dans la base de données. 
    // Elle retourne le nouvel id créé par la base de données.
    public function inscription($email, $name, $passwordHash, $birthdate){
        // Je prepare la requete
        $pdoStatement = $this->connexion->prepare(
            "INSERT INTO Users (email, name, password, birthdate) VALUES (:paramEmail, :paramName, :paramPassword, :paramBirthdate)");
        
        // J'exécute la requete
        // En lui passant les valeurs en paramètres
        $pdoStatement->execute(array(
            "paramEmail" => $email,
            "paramName" => $name,
            "paramPassword" => $passwordHash,
            "paramBirthdate" => $birthdate
        ));

        // Pour débugger et verifier que tout s'est bien pasé
        //var_dump($pdoStatement->errorInfo());

        // Je récupère l'id qui a été créé par la base de données
        $id = $this->connexion->lastInsertId();
        return $id;


    }

    // Cette fonction cherche un utilisateur par son email. Elle retourne un objet User.
    public function getUserByEmail($email){
        // Je prépare ma requete
        $pdoStatement = $this->connexion->prepare(
            "SELECT id, email, name, password, birthdate
            FROM Users
            WHERE email = :paramEmail"
        );

        // J'exécute la requete
        $pdoStatement->execute(
            array("paramEmail" => $email)
        );

        // Je recupere et je stocke le resultat
        $user = $pdoStatement->fetchObject("User");
        //var_dump($monChien);
        return $user;
    }

    // Cette fonction cherche un utilisateur par son id. Elle retourne un objet User.
    public function getUserById($id){
        // Je prépare ma requete
        $pdoStatement = $this->connexion->prepare(
            "SELECT id, email, name, password, birthdate
            FROM Users
            WHERE id = :paramId"
        );

        // J'exécute la requete
        $pdoStatement->execute(
            array("paramId" => $id)
        );

        // Je recupere et je stocke le resultat
        $user = $pdoStatement->fetchObject("User");
        //var_dump($monChien);
        return $user;
    }

    public function getEmailOccurences($email){
        // Je prépare ma requete
        $pdoStatement = $this->connexion->prepare(
            "SELECT COUNT(*)
            FROM Users
            WHERE email = :paramEmail"
        );

        // J'exécute la requete
        $pdoStatement->execute(
            array("paramEmail" => $email)
        );

        // Je recupere et je stocke le resultat
        $result = $pdoStatement->fetch();
        //var_dump($monChien);
        return $result[0];
    }

}

?>