<?php

class User {

    // Attributs
    private $id;
    private $email;
    private $name;
    private $password;
    private $birthdate;


    // Constructeur par défaut

    // Fonctions
    public function __set($name, $value){}
    public function getId(){
        return $this->id;
    }
    public function getEmail(){
        return $this->email;
    }
    public function getName(){
        return $this->name;
    }
    public function getPassword(){
        return $this->password;
    }
    public function getBirthdate(){
        return $this->birthdate;
    }
    
}

?>