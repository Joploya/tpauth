
# TP PHP : authentifier les utilisateurs d’un site web

## Objectif :

On veut un site web qui affiche une page personnelle en fonction de l’utilisateur authentifié. La page personnelle doit simplement afficher un message d’accueil à l’utilisateur en fonction de son nom et indiquer combien de jours il a passé sur terre en fonction de sa date de naissance.

Il faut une page d’inscription qui demande les informations suivantes :
- Adresse email
- Prénom
- Mot de passe
- Date de naissance

Cette page doit permettre de créer un nouvel utilisateur et l’insérer en base de données.

On a également besoin d’une page de login qui demande l’adresse email et le mot de passe. Si l’authentification réussi, la page personnelle de l’utilisateur est affichée sinon il reste sur la page login.

La page d’accueil du site doit être la page de login. Depuis cette page l’utilisateur non inscrit doit pouvoir accéder à la page d’inscription. Lorsqu’il s’inscrit il est redirigé vers la page login. S’il s’authentifie avec succès il est redirigé vers sa page personnelle. Depuis cette page il doit pouvoir se déconnecter et être redirigé vers la page login.

## Etape 1 : installation du projet

Créez l’environnement du projet. Tout d’abord téléchargez le zip d’un projet docker (stack LAMP) vierge à cette adresse :

[https://gitlab.com/realise-ge/containerized-lamp](https://gitlab.com/realise-ge/containerized-lamp)

  

Dézippez le projet vierge et renommez-le tp-authentification.

Ouvrez le projet dans Visual Studio et depuis le terminal lancez un docker-compose up afin de démarrer les différents serveurs.

Allez ensuite sur PhpMyAdmin et créez une base de données pour le site grâce au script suivant :

    CREATE  DATABASE tpAuth;
    USE tpAuth;
    
    CREATE USER 'adminAuth'@'%' IDENTIFIED BY  'adm1n@th';
    GRANT ALL PRIVILEGES ON tpAuth.*  TO  'adminAuth'@'%';
    
    CREATE  TABLE Users(
    	id BIGINT  PRIMARY  KEY AUTO_INCREMENT,
    	email VARCHAR(255),
    	name  VARCHAR(255),
    	password  VARCHAR(255),
    	birthdate DATE
    );

  
## Etape 2 : outils

Créez la classe User qui contient les attributs id, email, name, password, birthdate. N’oubliez pas les getters.

Créez la classe Database avec l’attribut connexion et le constructeur qui permet débuter une connexion avec la base de données.

Dans la classe Database implémentez les fonctions suivantes :

    public function inscription($email, $name, $passwordHash, $birthdate)

Cette fonction permet d’insérer un nouvel utilisateur dans la base de données. Elle retourne le nouvel id créé par la base de données.

    public function getUserByEmail($email)

Cette fonction cherche un utilisateur par son email. Elle retourne un objet User.

    public function getUserById($id)

Cette fonction cherche un utilisateur par son id. Elle retourne un objet User.

## Etape 3 : inscription

Créez une page inscription.php qui affiche un formulaire demandant l’email, le prénom, le mot de passe et la date de naissance.

Lorsque l’utilisateur clique sur le bouton envoyer du formulaire celui-ci envoie toutes les informations à la page process-inscription.php.

Sur cette page intermédiaire il faut :

- Récupérer les données du formulaire ($_POST)

- Hasher le mot de passe et le stocker dans une nouvelle variable (fonction password_hash).

- Instancier un objet Database et appeler la fonction inscription en lui passant les données du formulaire d’incription ainsi que le mot de passe hashé au lieu du mot de passe en clair.

- Rediriger vers la page de login.

## Etape 4 : login

Créez une page login.php qui affiche un formulaire de login (email, password).

Lorsque le formulaire est validé, les informations sont envoyées à la page process-login.php.

Sur cette page intermediaire il faut :

- Récupérer l’email et le mot de passe du formulaire ($_POST)

- Instancier une nouvelle Database et appeler la fonction getUserByEmail en lui passant l’email précédemment récupéré. Cette fonction nous renvoie toutes les informations dans un objet User qu’il faut stocker dans une nouvelle variable.

- Vérifiez que le mot de passe (user->getPassword()) du user n’est pas null. Si c’est le cas, renvoyer (header…) sur la page login.

- Authentifiez l’utilisateur grâce au mot de passe entré dans le formulaire, en le comparant au mot de passe de l’objet User, à l’aide de la fonction password_verify.

- Si la fonction password_verify retourne true, stockez l’id du user dans la session ($_SESSION) et redirigez vers la page perso.php sinon renvoyez si la page login.php

## Etape5 : page personnelle

La page perso.php doit afficher Bienvenue « *prénom* », vous êtes sur terre depuis « *X* » jours. Ainsi qu’un bouton pour fermer la session.

Pour pouvoir afficher ce message la première chose dont on a besoin est l’utilisateur.

Commencez votre page par un script php dans lequel vous implémentez les étapes suivantes :

- Démarrer la session (session_start)

- Récupérer l’id stocké dans la session ($_SESSION[‘’id’’])

- Vérifier que l’id n’est pas null ou vide. Si c’est le cas il faut renvoyer vers la page de login.

- Instancier une nouvelle Database et récupérer l’utilisateur en fonction de l’id de la session (getUserById). Stocker l’utiisateur dans une nouvelle variable.

- Il faut également stocker dans une variable le nombre de jour depuis la naissance de l’utilisateur. Pour cela :

		o Récupérer la date d’aujourd’hui

		o Trouver une fonction ou en écrire une qui donne le nombre de jours entre 2 dates

		o Stocker le résultat dans une nouvelle variable

A la suite de votre script php mettez votre code HTML qui affiche le message de bienvenue. Remplacez « *prénom* » et « *X* » par les valeurs stockée dans l’objet User et calculée dans le script php.

Enfin créez un bouton déconnexion qui envoie vers une page process-deconnexion.php.

Sur cette page appelez simplement la fonction session_destroy puis redirigez vers la page login.


## Pour aller plus loin

### Unicité de l'inscription

Il serait utile de vérifier qu'une personne n'essaye pas de s'inscrire plusieurs fois avec le même email car dans ce cas notre système d'authentification échouerait.

Il faut apporter des modifications dans la classe Database et dans la page process-inscription.php.

On a besoin d'une fonction qui compte combien de fois l'email apparait dans notre table en base de données. 
- 0 fois => on peut faire l'inscription
- 1 fois => inscription impossible
- plusieurs fois => la base est corrompue, on utilisera le premier utilisateur trouvé avec cet email.

Ecrire une fonction dans la classe Database qui compte le nombre d'emails identiques à celui passé en paramètre:
        public function getEmailOccurences($email)

Cette fonction exécute la requête suivante en base de données :
        SELECT COUNT(*)
        FROM Users
        WHERE email = "test@remplacer.com"

La requête doit être modifier pour prendre l'email en paramètre variable.

Dans process-inscription.php, avant d'appeler la fonction inscription, appelez la fonction getEmailOccurences. Si le resultat est strictement supérieur  0 redirigez vers la page inscription.php

### Transmission de messages d'erreur

Lorsqu'on est redirigé vers les inscription.php et login.php, s'il y a eu des erreurs lors de l'envoie du formulaire on aimerait le savoir. 

Il y a plusieurs techniques pour renvoyer le message en utilisant une requête HTTP POST à cette adresse :
[https://thisinterestsme.com/php-redirecting-a-form-with-post-variables/](https://thisinterestsme.com/php-redirecting-a-form-with-post-variables/)

Nous allons nous contenter d'envoyer les erreurs via les paramètres du requête HTTP GET (partie derrière le ? dans l'url).

Dans la page process-inscription.php, stocker les messages d'erreur dans une variable $error. 
A chaque fois que vous appelez la fonction header pour rediriger vers inscription.php, concaténez "?error=$error" à l'url.

Ensuite dans la page inscription.php utilisez une div avec la classe alert-danger pour afficher le message d'erreur que vous aurez récupérer dans $_GET["error].

Procédez de même pour process-login.php et login.php
