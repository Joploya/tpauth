CREATE DATABASE tpAuth;

USE tpAuth;

CREATE USER 'adminAuth'@'%' IDENTIFIED BY 'adm1n@th';
GRANT ALL PRIVILEGES ON tpAuth.* TO 'adminAuth'@'%';

CREATE TABLE Users(
    id BIGINT PRIMARY KEY AUTO_INCREMENT,
    email VARCHAR(255),
    name VARCHAR(255),
    password VARCHAR(255),
    birthdate DATE
);

